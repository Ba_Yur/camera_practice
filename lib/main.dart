import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'dart:io';

List<CameraDescription> cameras;

Future <void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  cameras = await availableCameras();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String path;
  CameraController controller;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (controller == null || !controller.value.isInitialized) {
      return;
    }
    if (state == AppLifecycleState.inactive) {
      controller?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      if (controller != null) {
      }
    }
  }

  void initState() {
    super.initState();
    controller = CameraController(cameras[0], ResolutionPreset.max);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!controller.value.isInitialized) {
      return Container();
    }
    return MaterialApp(
      home: Stack(
        children: [
          path == null ? CameraPreview(controller) : Image.file(File(path)),
          Align(
            alignment: Alignment.bottomCenter,
            child: FloatingActionButton(onPressed: () async {
              var value = await controller.takePicture();
              setState(() {
                path = value.path;
              });
            },
              child: Icon(Icons.photo_camera_outlined),
            ),
          ),
        ],
      ),
    );
  }
}
